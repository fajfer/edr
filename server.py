import argparse
import os
import socket
import json
from math import floor
from pathlib import Path
from platform import system
from subprocess import check_output
from threading import Thread
from time import sleep
from typing import Type

from blessed import Terminal
from pydantic import BaseModel

from endpoints import generate_endpoints_view
from menu import c_sunny, generate_control_bar, generate_menu, set_scrolling_area
from window import generate_window

PROJECTPATH = Path(__file__).resolve().parent
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

window_active = False
active_connections = 0
selection = 0
global_message_list = []
app_msg_indices = []
sec_msg_indices = []
sys_msg_indices = []
srv_msg_indices = []

active_addresses = []
open_sockets = []


def timestamp_gen() -> str:
    return (
        check_output("LANG=en_us date -u +'%Y-%b-%d %H:%M:%S'", shell=True)
        .decode()
        .rstrip()
    )


def parse_date(date: str) -> str:
    """Parse Windows date to WUT-EDR date type"""
    dt = date.split(" ")
    return f"{dt[4]}-{dt[1]}-{dt[2]} {dt[3]}"


def short_msg(message) -> str:
    if "\n" in message:
        return message.splitlines()[0][
            : (floor(Terminal.width) - floor(3 * Terminal.width / 6))
        ]
    return message[: (floor(Terminal.width) - floor(3 * Terminal.width / 6))]


class Message(BaseModel):
    os: str
    type: str
    date: str
    endpoint: str
    event_id: str
    event_type: str
    source: str
    message: str


def new_msg(
    Terminal,
    message,
    endpoint="localhost",
    source="Server",
    os=system(),
    type="Server",
    event_id=0,
    event_type="other",
    **kwargs,
) -> None:
    if "Server" in type and "localhost" not in endpoint:
        return
    if "Windows" in os and message == "":
        return

    set_date = True
    for key in kwargs:
        if "date" in key:
            set_date = False

    if set_date:
        date = timestamp_gen()
    else:
        date = kwargs["date"]

    global_message_list.append(
        Message(
            os=os,
            type=type,
            date=date,
            endpoint=endpoint,
            event_id=event_id,
            event_type=event_type,
            source=source,
            message=message,
        )
    )

    if global_message_list[-1].os == "Windows":
        global_message_list[-1].date = parse_date(global_message_list[-1].date)

    with open(f"{PROJECTPATH}/edr.log", "a+") as log:
        log.write(f"{global_message_list[-1]}\n")

    if "Security" in global_message_list[-1].type:
        sec_msg_indices.append(len(global_message_list) - 1)
    elif "System" in global_message_list[-1].type:
        sys_msg_indices.append(len(global_message_list) - 1)
    elif "Application" in global_message_list[-1].type:
        app_msg_indices.append(len(global_message_list) - 1)
    elif "Server" in global_message_list[-1].type:
        srv_msg_indices.append(len(global_message_list) - 1)

    if (
        selection < Terminal.height - 4
        and len(global_message_list) > Terminal.height - 4
    ):
        return
    elif window_active:
        return
    else:
        term_print(Terminal, global_message_list[-1])


def term_print(Terminal, message) -> None:
    if "\n" in message.message:
        print(
            f"{message.date[: floor(Terminal.width / 6)]}"
            + Terminal.move_x(floor(Terminal.width / 6))
            + f"{message.endpoint[: floor(2 * Terminal.width / 6)]}"
            + Terminal.move_x(floor(2 * Terminal.width / 6))
            + f"{message.type[: floor(3 * Terminal.width / 6)]}"
            + Terminal.move_x(floor(3 * Terminal.width / 6))
            + message.message.splitlines()[0][
                : (floor(Terminal.width) - floor(3 * Terminal.width / 6))
            ]
        )
    else:
        print(
            f"{message.date[: floor(Terminal.width / 6)]}"
            + Terminal.move_x(floor(Terminal.width / 6))
            + f"{message.endpoint[: floor(2 * Terminal.width / 6)]}"
            + Terminal.move_x(floor(2 * Terminal.width / 6))
            + f"{message.type[: floor(3 * Terminal.width / 6)]}"
            + Terminal.move_x(floor(3 * Terminal.width / 6))
            + message.message[: (floor(Terminal.width) - floor(3 * Terminal.width / 6))]
        )


def update_connections(Terminal, active_connections) -> None:
    with Terminal.location():
        print(
            Terminal.home
            + Terminal.on_color_rgb(*c_sunny)
            + Terminal.black
            + f"Active Connections: {active_connections}"
        )


def show_filtered_items(term, indices):
    for index in indices[0 : selection + term.height]:
        if selection < term.height - 4:
            if indices.index(index) > selection + term.height - 5:
                pass
            else:
                if (
                    global_message_list[index]
                    == global_message_list[indices[selection]]
                ):
                    print(f"{term.bold_red}", end="")
                    term_print(term, global_message_list[index])
                else:
                    print(f"{term.normal}", end="")
                    term_print(term, global_message_list[index])
        else:
            if indices.index(index) < selection:
                pass
            else:
                if (
                    global_message_list[index]
                    == global_message_list[indices[selection]]
                ):
                    print(f"{term.bold_red}", end="")
                    term_print(term, global_message_list[index])
                else:
                    print(f"{term.normal}", end="")
                    term_print(term, global_message_list[index])


def select_item(term, active_connections, event_filter):
    generate_menu(term, active_connections)
    set_scrolling_area(term, 2, term.height - 2)
    generate_control_bar(term, len(global_message_list))

    if event_filter == 0:
        pass
    elif event_filter == 1:
        show_filtered_items(term, app_msg_indices)
        return
    elif event_filter == 2:
        show_filtered_items(term, sec_msg_indices)
        return
    elif event_filter == 3:
        show_filtered_items(term, sys_msg_indices)
        return
    elif event_filter == 4:
        show_filtered_items(term, srv_msg_indices)
        return

    for message in global_message_list[selection : selection + term.height]:
        if selection < term.height - 4:
            if global_message_list.index(message) > selection + term.height - 5:
                pass
            else:
                if message == global_message_list[selection]:
                    print(f"{term.bold_red}", end="")
                    term_print(term, message)
                else:
                    print(f"{term.normal}", end="")
                    term_print(term, message)
        else:
            if global_message_list.index(message) < selection:
                pass
            else:
                if message == global_message_list[selection]:
                    print(f"{term.bold_red}", end="")
                    term_print(term, message)
                else:
                    print(f"{term.normal}", end="")
                    term_print(term, message)


def main(Terminal) -> None:
    global window_active
    global global_message_list
    global app_msg_indices
    global sec_msg_indices
    global sys_msg_indices
    global srv_msg_indices
    global selection
    global active_connections
    parser = argparse.ArgumentParser(description="WUT-EDR server")
    parser.add_argument(
        "--port",
        "-p",
        dest="port",
        default=63537,
        type=int,
        help="port on which server is listening (default: 63537)",
    )
    parser.add_argument("host", type=str, help="socket to bind the host")
    args = parser.parse_args()
    event_filter = 0
    generate_menu(term, active_connections)
    set_scrolling_area(term, 2, term.height - 2)
    generate_control_bar(term, len(global_message_list))

    try:
        serverSocket.bind((args.host, args.port))
        new_msg(term, f"Bind socket to host {args.host}")
    except socket.error as error:
        exit(f"[ERROR] Failed to bind the socket:\n\033[31m{error}\033[0m")
    new_msg(term, f"Listening on port {args.port}... (Waiting for connections)")
    select_item(term, active_connections, event_filter)
    serverSocket.listen(50)
    for clientSocket in open_sockets:
        clientSocket.close()
        del open_sockets[:], active_addresses[:]

    Thread(target=getClients, args=(Terminal, args.host, args.port)).start()

    while True:
        with term.cbreak():
            event = term.inkey()
            if event:
                if event.name == "KEY_PGUP":
                    selection = 0
                if event.name == "KEY_PGDOWN":
                    if event_filter == 0:
                        selection = len(global_message_list) - 1
                    elif event_filter == 1:
                        selection = len(app_msg_indices) - 1
                    elif event_filter == 2:
                        selection = len(sec_msg_indices) - 1
                    elif event_filter == 3:
                        selection = len(sys_msg_indices) - 1
                    elif event_filter == 4:
                        selection = len(srv_msg_indices) - 1
                if event.name == "KEY_UP":
                    selection -= 1
                    Terminal.move_y(selection)
                if event.name == "KEY_DOWN":
                    selection += 1
                    Terminal.move_y(selection)
                if event.name == "KEY_ENTER":
                    window_active = True
                    if event_filter == 0:
                        generate_window(
                            term, global_message_list[selection], window_active
                        )
                    elif event_filter == 1:
                        generate_window(
                            term,
                            global_message_list[app_msg_indices[selection]],
                            window_active,
                        )
                    elif event_filter == 2:
                        generate_window(
                            term,
                            global_message_list[sec_msg_indices[selection]],
                            window_active,
                        )
                    elif event_filter == 3:
                        generate_window(
                            term,
                            global_message_list[sys_msg_indices[selection]],
                            window_active,
                        )
                    elif event_filter == 4:
                        generate_window(
                            term,
                            global_message_list[srv_msg_indices[selection]],
                            window_active,
                        )
                if event == "0":
                    event_filter = 0
                    selection = 0
                if event == "1":
                    event_filter = 1
                    selection = 0
                if event == "2":
                    event_filter = 2
                    selection = 0
                if event == "3":
                    event_filter = 3
                    selection = 0
                if event == "4":
                    event_filter = 4
                    selection = 0
                if event == "x":
                    event_filter = 0
                    selection = 0
                    global_message_list = []
                    new_msg(term, "Messages deleted")
                if event == "e":
                    window_active = True
                    generate_endpoints_view(
                        term, active_addresses, active_connections, window_active
                    )
                if event == "q":
                    term.clear()
                    return
            event = None
            if event_filter == 0:
                if len(global_message_list) > 0:
                    selection = selection % len(global_message_list)
            elif event_filter == 1:
                if len(app_msg_indices) > 0:
                    selection = selection % len(app_msg_indices)
            elif event_filter == 2:
                if len(sec_msg_indices) > 0:
                    selection = selection % len(sec_msg_indices)
            elif event_filter == 3:
                if len(sys_msg_indices) > 0:
                    selection = selection % len(sys_msg_indices)
            elif event_filter == 4:
                if len(srv_msg_indices) > 0:
                    selection = selection % len(srv_msg_indices)

            select_item(term, active_connections, event_filter)


def getClients(term, server_host, server_port) -> None:
    global active_connections
    while True:
        try:
            conn, (address, port) = serverSocket.accept()
            open_sockets.append(conn)

            connName = f"{address}:{port}"
            new_msg(term, "Client connected!", connName)
            welcomeMessage = (
                f"Successfully connected to EDR Server at {server_host}:{server_port}"
            )

            conn.send(welcomeMessage.encode())
            active_connections += 1
            active_addresses.append(connName)
            update_connections(term, active_connections)
            Thread(target=handleClient, args=(conn, connName, term)).start()
            Thread(target=checkConnections, args=("", term)).start()
        except socket.error as acceptError:
            new_msg(
                term,
                f"Error accepting Connection from: {conn.getpeername()}. {acceptError}",
                connName,
            )
            continue


def handleClient(conn, connName, term) -> Message:
    while True:
        try:
            message = conn.recv(4096).decode()
            if message == "":
                pass
            else:
                try:
                    msg = json.loads(message)
                    new_msg(
                        term,
                        msg["message"],
                        source=msg["source"],
                        os=msg["os"],
                        type=msg["type"],
                        event_id=msg["event_id"],
                        event_type=msg["event_type"],
                        endpoint=connName,
                        date=msg["date"],
                    )
                except json.JSONDecodeError as err:
                    pass
        except ConnectionResetError:
            pass


def checkConnections(ws, term) -> None:
    global active_connections
    while True:
        if len(open_sockets) != 0:
            for x, currentSocket in enumerate(open_sockets):
                try:
                    pingToClientMessage = " "
                    currentSocket.send(pingToClientMessage.encode())
                except:
                    new_msg(term, "Client disconnected!", active_addresses[x])
                    del open_sockets[x], active_addresses[x]
                    active_connections -= 1
                    update_connections(term, active_connections)
                    if active_connections == 0:
                        new_msg(term, "No active connections left.")
                    continue
        sleep(30)


if __name__ == "__main__":
    term = Terminal()
    with term.fullscreen():
        main(term)
    os._exit(0)
