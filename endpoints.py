from blessed import Terminal
from math import floor

from menu import generate_menu, set_scrolling_area, c_minty


def print_endpoints(
    Terminal, active_addresses, endpoint_selector, active_connections
) -> None:
    generate_menu(Terminal, active_connections)

    # Generate status bar
    print(
        Terminal.move_y(1)
        + Terminal.clear_eol
        + Terminal.on_color_rgb(*c_minty)
        + Terminal.center(""),
        end="",
    )

    print(
        Terminal.move_yx(1, 0)
        + Terminal.on_color_rgb(*c_minty)
        + Terminal.black
        + "ENDPOINT"
    )
    set_scrolling_area(Terminal, 2, Terminal.height - 2)

    with Terminal.location():
        print(
            Terminal.move_xy(0, Terminal.height - 1)
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.center(""),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2("<x> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Execute"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <c> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Config"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <t> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Terminate"),
            end="",
        )

    for endpoint in active_addresses:
        if endpoint_selector < Terminal.height - 4:
            if (
                active_addresses.index(endpoint)
                > endpoint_selector + Terminal.height - 5
            ):
                pass
            else:
                if endpoint == active_addresses[endpoint_selector]:
                    print(Terminal.on_black + Terminal.bold_red, end="")
                    print(f"{endpoint}")
                else:
                    print(Terminal.on_black + Terminal.snow, end="")
                    print(f"{endpoint}")
        else:
            if endpoint == active_addresses[endpoint_selector]:
                print(f"{Terminal.bold_red}", end="")
                print(f"{endpoint}")
            else:
                print(Terminal.on_black + Terminal.snow, end="")
                print(f"{endpoint}")


def generate_endpoints_view(
    Terminal, active_addresses, active_connections, active
) -> None:
    endpoint_selector = 0
    print_endpoints(Terminal, active_addresses, endpoint_selector, active_connections)

    with Terminal.cbreak():
        while active:
            event = Terminal.inkey()
            if event.name == "KEY_PGUP":
                endpoint_selector = 0
            if event.name == "KEY_PGDOWN":
                endpoint_selector = len(active_addresses) - 1
            if event.name == "KEY_UP":
                endpoint_selector -= 1
                Terminal.move_y(endpoint_selector)
            if event.name == "KEY_DOWN":
                endpoint_selector += 1
                Terminal.move_y(endpoint_selector)
            if event == "x":
                pass
            if event == "c":
                pass
            if event == "t":
                pass
            if event == "q" or event.name == "KEY_ESCAPE":
                active = False
            if len(active_addresses) > 0:
                endpoint_selector = endpoint_selector % len(active_addresses)
            print_endpoints(
                Terminal, active_addresses, endpoint_selector, active_connections
            )
