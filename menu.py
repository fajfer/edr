from blessed import Terminal
from math import floor

## Warsaw University of Technology colour palette:
c_sunny = [254, 216, 69]
c_minty = [104, 188, 155]
c_plummy = [178, 161, 171]


def generate_menu(Terminal, active_connections) -> None:
    endpoint_column = floor(Terminal.width / 6)
    logtype_column = floor(2 * Terminal.width / 6)
    data_column = floor(3 * Terminal.width / 6)

    # Clear the terminal
    print(Terminal.clear())

    # Generate title
    print(
        Terminal.home
        + Terminal.on_color_rgb(*c_sunny)(
            Terminal.center(Terminal.black(Terminal.bold("WUT-EDR v0.3")))
        ),
        end="",
    )
    print(
        Terminal.home
        + Terminal.on_color_rgb(*c_sunny)
        + Terminal.black
        + f"Active Connections: {active_connections}"
    )

    # Generate status bar
    print(Terminal.on_color_rgb(*c_minty) + Terminal.center(""))
    print(Terminal.move_y(1) + Terminal.black("TIME"), end="")
    print(
        Terminal.move_x(endpoint_column)
        + Terminal.on_color_rgb(*c_minty)
        + Terminal.black("ENDPOINT"),
        end="",
    )
    print(
        Terminal.move_x(logtype_column)
        + Terminal.on_color_rgb(*c_minty)
        + Terminal.black("TYPE"),
        end="",
    )
    print(
        Terminal.move_x(data_column)
        + Terminal.on_color_rgb(*c_minty)
        + Terminal.black("DATA")
    )


def set_scrolling_area(Terminal, x, y) -> None:
    with Terminal.location():
        print(Terminal.csr(x, y))
    return


def generate_control_bar(Terminal, messages) -> None:
    with Terminal.location():
        print(
            Terminal.move_xy(0, Terminal.height - 1)
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.center(""),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.move_x(Terminal.width - 6)
            + Terminal.rjust(Terminal.black("Messages: "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black(f"{messages}"),
            end="",
        )

        print(
            Terminal.move_xy(0, Terminal.height - 1)
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2("<0> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("All"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <1> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Application"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <2> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Security"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <3> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("System"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <4> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Server"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <e> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Endpoints"),
            end="",
        )
        print(
            Terminal.on_color_rgb(*c_minty)
            + Terminal.bold(Terminal.red2(" <x> "))
            + Terminal.on_color_rgb(*c_minty)
            + Terminal.black("Flush events"),
            end="",
        )
