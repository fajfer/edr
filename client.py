from os import close, path, remove
from platform import system
from socket import AF_INET, SOCK_STREAM, error, socket
from subprocess import check_output, run
from threading import Thread, Timer
from time import sleep

from scapy.all import DNS, sniff

import argparse
import os
import json

if system() == "Windows":
    import win32con
    import win32evtlog
    import win32evtlogutil
    import winerror


def check_register_changes(base_path: str, logtypes: list[str], interval: int) -> None:
    while True:
        for type in logtypes:
            print(f"Checking changes in {type}")
            with open(f"{base_path}\\{type}", "r") as origin:
                with open(f"{base_path}\\{type}-cmp", "r") as cmp:
                    difference = set(origin).difference(cmp)

            with open(f"{base_path}\\{type}-cmp", "a") as cmp:
                for message in difference:
                    cmp.write(message)
                    data = json.dumps(message)
                    msg = str.encode(data)
                    clientSocket.sendall(msg),
        print(f"Waiting: {interval}s")
        sleep(interval)


def getAllEvents(base_path: str, logtypes: list[str], sleep_interval: int):
    """ """
    getEventLogs(logtypes, base_path)
    for type in logtypes:
        open(f"{base_path}\\{type}-cmp", "w").close()
    check_register_changes(base_path, logtypes, sleep_interval)


def getEventLogs(logtypes, logPath):
    """
    Get the event logs from the specified machine according to the
    logtype (Example: Application) and save it to the appropriately
    named log file
    """
    for logtype in logtypes:
        print(f"{logPath}")
        print(f"Writing {logtype} events")
        open(f"{logPath}\\{logtype}", "w").close()

        hand = win32evtlog.OpenEventLog(None, logtype)
        total = win32evtlog.GetNumberOfEventLogRecords(hand)
        print("Total events in %s = %s" % (logtype, total))
        flags = (
            win32evtlog.EVENTLOG_BACKWARDS_READ | win32evtlog.EVENTLOG_SEQUENTIAL_READ
        )
        events = win32evtlog.ReadEventLog(hand, flags, 0)
        evt_dict = {
            win32con.EVENTLOG_AUDIT_FAILURE: "EVENTLOG_AUDIT_FAILURE",
            win32con.EVENTLOG_AUDIT_SUCCESS: "EVENTLOG_AUDIT_SUCCESS",
            win32con.EVENTLOG_INFORMATION_TYPE: "EVENTLOG_INFORMATION_TYPE",
            win32con.EVENTLOG_WARNING_TYPE: "EVENTLOG_WARNING_TYPE",
            win32con.EVENTLOG_ERROR_TYPE: "EVENTLOG_ERROR_TYPE",
        }

        while events:
            events = win32evtlog.ReadEventLog(hand, flags, 0)

            for ev_obj in events:
                the_time = ev_obj.TimeGenerated.Format()  #'12/23/99 15:54:09'
                evt_id = str(winerror.HRESULT_CODE(ev_obj.EventID))
                msg = win32evtlogutil.SafeFormatMessage(ev_obj, logtype)

                source = str(ev_obj.SourceName)
                if not ev_obj.EventType in evt_dict.keys():
                    evt_type = "unknown"
                else:
                    evt_type = str(evt_dict[ev_obj.EventType])
                data = {
                    "message": msg,
                    "source": source,
                    "os": "Windows",
                    "type": logtype,
                    "date": the_time,
                    "event_id": evt_id,
                    "event_type": evt_type,
                }
                try:
                    data_string = json.dumps(data)
                    data = str.encode(data_string)
                    clientSocket.sendall(data)
                    with open(f"{logPath}\\{logtype}", "a") as file:
                        file.write(f"{data}\n")
                except error as err:
                    print(f"{err.__cause__}")
                    pass

        print("Log creation finished. Location of log is %s" % logPath)


log_path = os.path.join("C:\\", "WUT-EDR")
if not os.path.exists(log_path):
    os.makedirs(log_path)

logtypes = ["System", "Application", "Security"]
restricted_site_list = ["pw.edu.pl", "xx1.es", "fajfer.org"]
ignored_macs = ["ff-ff-ff-ff-ff-ff"]


def main():
    parser = argparse.ArgumentParser(description="WUT-EDR client")
    parser.add_argument(
        "--port",
        "-p",
        dest="port",
        default=63537,
        type=int,
        help="port on which the client should connect to the server (default: 63537)",
    )
    parser.add_argument("host", type=str, help="server IP address")
    args = parser.parse_args()
    global clientSocket
    clientSocket = socket(AF_INET, SOCK_STREAM)

    print("Connecting...")
    try:
        clientSocket.connect((args.host, args.port))
        print(f"Successfully connected to: {args.host}:{args.port}.")
    except error as err:
        exit(f"Connection failed:\n\033[31m{err}\033[0m")


def MITM():
    while True:
        macList = []
        macDict = {}
        ARPmacs = check_output("arp -a", shell=True).decode()

        for line in ARPmacs.splitlines():
            macList.append(line[24:41])

        for MAC in macList:
            if MAC in macDict:
                macDict[MAC] = macDict[MAC] + 1
            else:
                macDict[MAC] = 1

        for MAC, occurences in macDict.items():
            if occurences >= 2:
                if MAC not in ignored_macs:
                    clientSocket.send(
                        f"MAC duplicates. Possible MITM attack.\nFound MAC address duplication. Possible Man in the Middle Attack!\nCheck this MAC: {MAC}\n\n".encode()
                    )
        sleep(15)


def findDNS(pkt):
    if pkt.haslayer(DNS):
        if "Qry" in pkt.summary():
            url = pkt.summary().split('"')[-2].replace("", "")[2:-2]
            for site in restricted_site_list:
                if site in url:
                    clientSocket.send(
                        f"Accessed a restricted website\n{site}\n\n".encode()
                    )


if __name__ == "__main__":
    main()
    base_path = "C:\\WUT-EDR"

    for type in logtypes:
        open(f"{base_path}\\{type}-cmp", "w").close()

    if system() == "Windows":
        Thread(target=getAllEvents(base_path, logtypes, 15)).start()

    if system() == "Linux":
        sleep(10)

    Thread(target=MITM).start()
    Thread(target=sniff(prn=findDNS)).start()
