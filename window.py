from blessed import Terminal
from math import floor


def generate_window(Terminal, message, active) -> None:
    print(
        Terminal.clear
        + Terminal.red_on_black("Date: ")
        + f"{message.date}\n"
        + Terminal.red_on_black("Endpoint: ")
        + f"{message.endpoint}\n"
        + Terminal.red_on_black("Type: ")
        + f"{message.type}\n"
        + Terminal.red_on_black("Event type: ")
        + f"{message.event_type}\n"
        + Terminal.red_on_black("Event id: ")
        + f"{message.event_id}\n"
        + Terminal.red_on_black("Source: ")
        + f"{message.source}\n"
        + Terminal.red_on_black("OS: ")
        + f"{message.os}\n"
        + Terminal.red_on_black("Message: ")
        + f"{message.message}\n"
        + "\n\nPress ESC key",
        end="",
    )
    with Terminal.cbreak():
        while active:
            if Terminal.inkey().name == "KEY_ESCAPE":
                active = False
